import unittest

from main import bd_happened

class TestBDHappened(unittest.TestCase):

    """
        Test that BDHappended function actually gives a correct answer
    """
    def test_date_has_not_happened(self):
        """
        Test that a correct inputed data is correctly filtered

        :return:
        """

        date_now = [2018, 1, 17]
        birth_date = [1980, 11, 12]

        bd_happened(date_now, birth_date)

        self.assertFalse(bd_happened(date_now, birth_date))

    def test_date_has_happened(self):
        """
        Test that a correct inputed data is correctly filtered

        :return:
        """

        date_now = [2018, 11, 17]
        birth_date = [1980, 1, 12]

        bd_happened(date_now, birth_date)

        self.assertTrue(bd_happened(date_now, birth_date))





if __name__ == '__main__':
    unittest.main()